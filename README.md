administrator@administrator:~$ git config --global user.name "Abhishek Hari"
administrator@administrator:~$ git config --global user.email "abhishekhari455@gmail.com"
administrator@administrator:~$ git clone https://gitlab.com/fosslab2018/4.4.LoopsandAdvancedShellScripting.git
Cloning into '4.4.LoopsandAdvancedShellScripting'...
warning: You appear to have cloned an empty repository.
Checking connectivity... done.
administrator@administrator:~$ cd 4.4.LoopsandAdvancedShellScripting
administrator@administrator:~/4.4.LoopsandAdvancedShellScripting$ touch README.md
administrator@administrator:~/4.4.LoopsandAdvancedShellScripting$ git add README.md
administrator@administrator:~/4.4.LoopsandAdvancedShellScripting$ git commit -m "add README"
[master (root-commit) 182c919] add README
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 README.md
administrator@administrator:~/4.4.LoopsandAdvancedShellScripting$ git push -u origin master
Username for 'https://gitlab.com': ASI16CS004
Password for 'https://ASI16CS004@gitlab.com': 
Counting objects: 3, done.
Writing objects: 100% (3/3), 221 bytes | 0 bytes/s, done.
Total 3 (delta 0), reused 0 (delta 0)
To https://gitlab.com/fosslab2018/4.4.LoopsandAdvancedShellScripting.git
 * [new branch]      master -> master
Branch master set up to track remote branch master from origin.
administrator@administrator:~/4.4.LoopsandAdvancedShellScripting$ touch loop.odt
administrator@administrator:~/4.4.LoopsandAdvancedShellScripting$ git add loop.odt
administrator@administrator:~/4.4.LoopsandAdvancedShellScripting$ git commit -m "FILE"
[master 388dada] FILE
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 loop.odt
administrator@administrator:~/4.4.LoopsandAdvancedShellScripting$ git push -u origin master
Username for 'https://gitlab.com': ASI16CS004
Password for 'https://ASI16CS004@gitlab.com': 
Counting objects: 3, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 290.70 KiB | 0 bytes/s, done.
Total 3 (delta 0), reused 0 (delta 0)
To https://gitlab.com/fosslab2018/4.4.LoopsandAdvancedShellScripting.git
   182c919..388dada  master -> master
Branch master set up to track remote branch master from origin.
administrator@administrator:~/4.4.LoopsandAdvancedShellScripting$ 